#MiniShop

Step 1:

```
composer install
```

Step 2. Database settings:

```
cp config/database.example.yaml config/database.yaml
```

And write your data in a new file.

Step 3. Install database schema:

```
composer db-schema-install
```

Step 4. Write data for example:

```
composer example-insert-products
```