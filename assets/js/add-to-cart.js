import * as $ from 'jquery';

$(document).ready(function () {

    const $button = $('.js-add-to-cart');

    $button.click(function () {
        const url = $(this).attr('href');
        const id = $(this).data('id');

        $.ajax({
            url: url,
            dataType: 'json',
            success: function (data) {
                const replace = data.replace;
                $(`#product-teaser-${id}`).replaceWith(replace);
            }
        });

        return false;
    });
});