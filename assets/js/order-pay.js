import * as $ from 'jquery';

$(document).ready(function () {

    const $button = $('.js-order-pay');

    $button.click(function () {
        const url = $(this).attr('href');
        const id = $(this).data('id');

        $('.spinner-border').removeClass('d-none');
        $(this).hide();

        $.ajax({
            url: url,
            dataType: 'json',
            success: function () {
                window.location.href = `/app/order/${id}`;
            }
        });

        return false;
    });
});