import * as $ from 'jquery';

$(document).ready(function () {

    const $button = $('.js-order-create');

    $button.click(function () {
        const url = $(this).attr('href');

        $('.spinner-border').removeClass('d-none');
        $(this).hide();

        $.ajax({
            url: url,
            dataType: 'json',
            success: function (data) {
                if(data.success) {
                    window.location.href = `/app/order/${data['order_id']}`;
                }
                else {
                    window.location.href = '/app/cart';
                }
            }
        });

        return false;
    });
});