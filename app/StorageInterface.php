<?php


namespace App;


interface StorageInterface {

  /**
   * @param int $id
   *
   * @return array
   */
  public function findId(int $id);

  /**
   * @return array
   */
  public function findAll();
}