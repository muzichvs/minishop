<?php


namespace App\Payment;


class YandexPayment {
  const PAY_URL = 'https://ya.ru';

  /**
   * @return bool
   */
  public function payOrder() {
    return $this->sendRequest() == 200;
  }

  /**
   * @return mixed
   */
  private function sendRequest() {
    $ch = curl_init(self::PAY_URL);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_NOBODY, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    curl_setopt($ch, CURLOPT_TIMEOUT,10);

    curl_exec($ch);
    $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    return $http_code;
  }
}