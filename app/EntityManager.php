<?php


namespace App;


use App\Entity\CartEntity;
use App\Entity\OrderEntity;
use App\Entity\ProductEntity;
use App\Storage\OrderStorage;
use App\Storage\ProductStorage;

class EntityManager {

  /**
   * @return \App\Entity\ProductEntity
   */
  public function product() {
    $storage = new ProductStorage();
    return new ProductEntity($storage);
  }

  /**
   * @return \App\Entity\CartEntity
   */
  public function cart() {
    return new CartEntity();
  }

  /**
   * @return \App\Entity\OrderEntity
   */
  public function order() {
    $storage = new OrderStorage();
    return new OrderEntity($storage);
  }
}