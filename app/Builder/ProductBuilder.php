<?php


namespace App\Builder;


use App\App;
use App\Builder;
use App\Entity\ProductEntity;

class ProductBuilder extends Builder {

  /**
   * @param array $items
   *
   * @return string
   */
  public function buildList(array $items) {
    return $this->renderComponent('product-list', [
      'items' => $items,
    ]);
  }

  /**
   * @param \App\Entity|\App\Entity\ProductEntity $product
   * @param bool $show_actions
   *
   * @return string
   */
  public function buildTeaser(ProductEntity $product, $show_actions = TRUE) {
    return $this->renderComponent('product-teaser', [
      'id' => $product->getPid(),
      'name' => $product->getName(),
      'price' => $product->getPrice(),
      'in_cart' => App::$entity->cart()->hasProduct($product),
      'show_actions' => $show_actions,
    ]);
  }
}