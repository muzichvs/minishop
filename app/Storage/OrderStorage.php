<?php


namespace App\Storage;


use App\App;
use App\StorageInterface;

class OrderStorage implements StorageInterface {

  /**
   * @param int $id
   *
   * @return array
   */
  public function findId(int $id) {
    $query = <<<SQL
SELECT * FROM `order`
WHERE oid = :id
SQL;

    return App::$database->execute($query, [
      ':id' => $id,
    ])->fetchAll();
  }

  /**
   * @return array
   */
  public function findAll() {
    $query = <<<SQL
SELECT * FROM `order`
SQL;

    return App::$database->execute($query)->fetchAll();
  }

  /**
   * @param int $order_id
   *
   * @return array
   */
  public function findItems(int $order_id) {
    $query = <<<SQL
SELECT pid FROM `order_item`
WHERE oid = :oid
SQL;

    return App::$database->execute($query, [
      ':oid' => $order_id,
    ])->fetchAll();
  }

  /**
   * @param $status
   *
   * @return string
   */
  public function insert($status) {
    $query = <<<SQL
INSERT INTO `order` (`status`, `created`)
VALUES (:status, :created);
SQL;

    $result = App::$database->execute($query, [
      ':status' => $status,
      ':created' => time(),
    ]);

    return $result ? App::$database->pdo->lastInsertId() : NULL;
  }

  /**
   * @param int $order_id
   * @param int $product_id
   *
   * @return string
   */
  public function insertItem(int $order_id, int $product_id) {
    $query = <<<SQL
INSERT INTO `order_item` (`oid`, `pid`)
VALUES (:oid, :pid);
SQL;

    $result = App::$database->execute($query, [
      ':oid' => $order_id,
      ':pid' => $product_id,
    ]);

    return $result ? App::$database->pdo->lastInsertId() : NULL;
  }

  /**
   * @param int $id
   * @param string $status
   *
   * @return bool|false|\PDOStatement
   */
  public function updateStatus(int $id, string $status) {
    $query = <<<SQL
UPDATE `order`
SET status = :status
WHERE oid = :id
SQL;

    return App::$database->execute($query, [
      ':id' => $id,
      ':status' => $status,
    ]);
  }
}