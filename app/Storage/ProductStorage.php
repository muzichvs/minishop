<?php


namespace App\Storage;

use App\App;
use App\DataBase\DataBase;
use App\StorageInterface;

class ProductStorage implements StorageInterface {

  /**
   * @param int $id
   *
   * @return array
   */
  public function findId(int $id) {
    $query = <<<SQL
SELECT * FROM `product`
WHERE pid = :id
SQL;

    return App::$database->execute($query, [
      ':id' => $id,
    ])->fetchAll();
  }

  /**
   * @return array
   */
  public function findAll() {
    $query = <<<SQL
SELECT * FROM `product`
SQL;

    return  App::$database->execute($query)->fetchAll();
  }

  /**
   * @param string $name
   * @param int $price
   */
  public function insert(string $name, int $price) {
    $query = <<<SQL
INSERT INTO `product` (`name`, `price`, `created`)
VALUES (:name, :price, :created);
SQL;

    (new DataBase())->execute($query, [
      ':name' => $name,
      ':price' => $price,
      ':created' => time(),
    ]);
  }
}