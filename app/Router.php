<?php


namespace App;


class Router {

  /**
   * @return array
   */
  public function compile() {
    $path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    $route = explode('/', $path);

    $params = isset($route[3]) ? array_slice($route, 3) : [];

    return [
      $route[1] ?? NULL,
      $route[2] ?? NULL,
      $params,
    ];
  }
}