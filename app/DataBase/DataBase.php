<?php


namespace App\DataBase;

use Symfony\Component\Yaml\Yaml;

class DataBase {

  /**
   * @var \PDO
   */
  public $pdo;

  /**
   * DataBase constructor.
   */
  public function __construct() {
    $settings = $this->getPDOSettings();
    $this->pdo = new \PDO($settings['dsn'], $settings['user'], $settings['pass'], NULL);
  }

  /**
   * @return mixed
   */
  protected function getPDOSettings() {
    if(!defined('ROOT_PATH')) define('ROOT_PATH', dirname(__DIR__, 2));

    $config = Yaml::parseFile(ROOT_PATH . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'database.yaml');

    $result['dsn'] = "{$config['type']}:host={$config['host']};dbname={$config['dbname']};charset={$config['charset']}";
    $result['user'] = $config['user'];
    $result['pass'] = $config['pass'];

    return $result;
  }

  /**
   * @param $query
   * @param array|NULL $params
   *
   * @return bool|false|\PDOStatement
   */
  public function execute($query, array $params = NULL) {
    if (is_null($params)) {
      $stmt = $this->pdo->query($query);
      return $stmt;
    }

    $stmt = $this->pdo->prepare($query);
    $stmt->execute($params);
    return $stmt;
  }
}