<?php


namespace App\DataBase;

use App\Storage\ProductStorage;

class Example {

  public static function insertProducts() {
    $products = [
      ['name' => 'Бензотриммер PATRIOT PT 545', 'price' => 6999],
      ['name' => 'Перфоратор Makita HR 2470', 'price' => 7449],
      ['name' => 'Газонокосилка Ryobi RLM13E33S 5133002343', 'price' => 6090],
      ['name' => 'Угловая шлифмашина DeWALT DWE8110S', 'price' => 4652],
      ['name' => 'Защитные открытые очки РОСОМЗ О35 ВИЗИОН PL 13511', 'price' => 94],
      ['name' => 'Аккумуляторная отвертка Bosch GO 0.601.9H2.020', 'price' => 2519],
      ['name' => 'Аккумуляторный винтоверт Metabo PowerMaxx BS Basic 600080500', 'price' => 5395],
      ['name' => 'Насос Могилев Ручеёк-1 10 м, коробка', 'price' => 1264],
      ['name' => 'Электрод ОК 46.00 (3 мм; 5.3 кг) ESAB СВ000007576', 'price' => 859],
    ];

    foreach ($products as $product) {
      (new ProductStorage())->insert($product['name'], $product['price']);
    }
  }
}