<?php

namespace App\DataBase;

class DataBaseSchema {

  /**
   * Install
   */
  public static function install() {
    $schema = new self();
    $schema->createTableProduct();
    $schema->createTableOrder();
    $schema->createTableOrderItem();
  }

  /**
   * Create product table
   */
  public function createTableProduct() {
    $query = <<<SQL
CREATE TABLE `product` (
  `pid` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NULL,
  `price` INT NULL,
  `created` INT NOT NULL,
  PRIMARY KEY (`pid`))
  CHARSET=utf8mb4;
SQL;

    (new DataBase())->execute($query);
  }

  /**
   * Create order table
   */
  public function createTableOrder() {
    $query = <<<SQL
CREATE TABLE `order` (
  `oid` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `status` VARCHAR(70) NULL,
  `created` INT NOT NULL,
  PRIMARY KEY (`oid`));
SQL;

    (new DataBase())->execute($query);
  }

  /**
   * Create order item table
   */
  public function createTableOrderItem() {
    $query = <<<SQL
CREATE TABLE `order_item` (
  `oid` INT NOT NULL,
  `pid` INT NOT NULL,
  PRIMARY KEY (`oid`, `pid`));
SQL;

    (new DataBase())->execute($query);
  }
}