<?php


namespace App;


abstract class Entity {

  /**
   * @var \App\StorageInterface
   */
  protected $storage;

  /**
   * Entity constructor.
   *
   * @param \App\StorageInterface $storage
   */
  public function __construct(StorageInterface $storage) {
    $this->storage = $storage;
  }

  /**
   * @param int $id
   *
   * @return \App\Entity|null
   */
  public function load(int $id) {
    $result = $this->storage->findId($id);
    if(empty($result)) {
      return NULL;
    }

    $values = current($result);

    return $this->setArray($values);
  }

  /**
   * @return array
   */
  public function loadAll() {
    $result = [];
    foreach ($this->storage->findAll() as $values) {
      $result[] = $this->setArray($values);
    }

    return $result;
  }

  /**
   * @param $values
   *
   * @return \App\Entity
   */
  public function setArray($values) {
    $entity = clone $this;
    foreach ($values as $name => $value) {
      $setter = 'set' . ucfirst($name);
      if (!method_exists($entity, $setter)) {
        continue;
      }

      $entity->$setter($value);
    }

    return $entity;
  }
}