<?php


namespace App;

use App\Exceptions\InvalidRouteException;

class Kernel {

  public $defaultControllerName = 'App';

  public $defaultActionName = "index";

  public function launch() {
    list($controllerName, $actionName, $params) = App::$router->compile();
    echo $this->launchAction($controllerName, $actionName, $params);
  }


  /**
   * @param $controllerName
   * @param $actionName
   * @param $params
   *
   * @return mixed
   * @throws \App\Exceptions\InvalidRouteException
   */
  public function launchAction($controllerName, $actionName, $params) {
    $controllerName = empty($controllerName) ? $this->defaultControllerName : ucfirst($controllerName);
    $controllerName.= 'Controller';

    if (!class_exists("\\App\\Controller\\" . ucfirst($controllerName))) {
      throw new InvalidRouteException();
    }

    $controllerName = "\\App\\Controller\\" . ucfirst($controllerName);
    $controller = new $controllerName;
    $actionName = empty($actionName) ? $this->defaultActionName : $actionName;

    if (!method_exists($controller, $actionName)) {
      throw new InvalidRouteException();
    }

    return $controller->$actionName($params);
  }
}