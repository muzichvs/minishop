<?php


namespace App\Controller;

use App\Controller;

class ErrorController extends Controller {

  /**
   * @return string
   */
  public function error404() {
    return $this->render('error/error404');
  }

  /**
   * @return string
   */
  public function error500() {
    return $this->render('error/error500');
  }
}