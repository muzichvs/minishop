<?php


namespace App\Controller;

use App\App;
use App\Builder\ProductBuilder;
use App\Controller;
use App\Entity\OrderEntity;

class AppController extends Controller {

  /**
   * Home
   *
   * @return string
   */
  public function index() {
    $products = App::$entity->product()->loadAll();

    $items = [];
    $product_builder = new ProductBuilder();
    foreach ($products as $product) {
      $items[] = $product_builder->buildTeaser($product);
    }

    return $this->renderComponent('home', [
      'products' => $product_builder->buildList($items)
    ]);
  }

  /**
   * Cart
   *
   * @return string
   */
  public function cart() {
    $cart_items = App::$entity->cart()->getProductIds();

    $items = [];
    $product_builder = new ProductBuilder();
    foreach ($cart_items as $product_id) {
      $product = App::$entity->product()->load($product_id);
      $items[] = $product_builder->buildTeaser($product, FALSE);
    }

    return $this->renderComponent('cart', [
      'products' => $product_builder->buildList($items)
    ]);
  }

  /**
   * Order
   *
   * @param $params
   *
   * @return string
   */
  public function order($params) {
    $order_id = $params[0] ?? NULL;
    if(!$order_id) {
      return (new ErrorController())->error404();
    }

    $order = App::$entity->order()->load($order_id);
    $products = $order->getProducts();

    $items = [];
    $product_builder = new ProductBuilder();
    foreach ($products as $product) {
      $items[] = $product_builder->buildTeaser($product, FALSE);
    }

    return $this->renderComponent('order', [
      'order_id' => $order_id,
      'products' => $product_builder->buildList($items),
      'show_actions' => $order->getStatus() == OrderEntity::STATUS_NEW,
      'status' => $order->getStatusName(),
    ]);
  }
}