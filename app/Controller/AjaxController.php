<?php

namespace App\Controller;

use App\App;
use App\Controller;
use App\Builder\ProductBuilder;
use App\Entity\OrderEntity;
use App\Payment\YandexPayment;

class AjaxController extends Controller {

  /**
   * @param $params
   *
   * @return false|string
   */
  public function addToCart($params) {
    $id = $params[0] ?? NULL;

    $product = App::$entity->product()->load($id);
    App::$entity->cart()->addProduct($product);
    return $this->jsonResponse(['replace' => (new ProductBuilder())->buildTeaser($product)]);
  }

  /**
   * @return false|string
   */
  public function createOrder() {
    $order_id = App::$entity->order()->create();
    if(!$order_id) {
      return $this->jsonResponse(['success' => 0]);
    }

    App::$entity->cart()->clear();

    return $this->jsonResponse([
      'success' => 1,
      'order_id' => $order_id
    ]);
  }

  /**
   * @param $params
   *
   * @return false|string
   */
  public function payOrder($params) {
    $id = $params[0] ?? NULL;

    if((new YandexPayment())->payOrder()) {
      App::$entity->order()
        ->load($id)
        ->setStatus(OrderEntity::STATUS_PAID)
        ->saveStatus()
      ;
    }

    return $this->jsonResponse([]);
  }
}