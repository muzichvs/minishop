<?php


namespace App;

use App\Controller\ErrorController;

class Controller extends Builder {

  /**
   * @param mixed $body
   *
   * @return string
   */
  public function renderLayout($body) {
    try {
      return $this->twig->render('layout.twig', ['body' => $body]);
    }
    catch (\Exception $exception) {
      return (new ErrorController())->error500();
    }
  }

  /**
   * @param string $templateName
   * @param array $params
   *
   * @return string
   */
  public function render(string $templateName, array $params = []) {
    $templateName.= '.twig';
    try {
      return $this->renderLayout($this->twig->render($templateName, $params));
    }
    catch (\Exception $exception) {
      return (new ErrorController())->error500();
    }
  }

  /**
   * @param array $value
   *
   * @return false|string
   */
  public function jsonResponse(array $value) {
    return json_encode($value);
  }
}