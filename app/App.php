<?php

namespace App;

class App {

  /**
   * @var \App\Router
   */
  public static $router;

  /**
   * @var \App\DataBase\DataBase
   */
  public static $database;

  /**
   * @var \App\Kernel
   */
  public static $kernel;

  /**
   * @var \App\EntityManager
   */
  public static $entity;

  /**
   * Init
   */
  public static function init() {
    static::bootstrap();
    set_exception_handler(['\App\App', 'handleException']);
  }

  /**
   * Bootstrap
   */
  public static function bootstrap() {
    static::$router = new Router();
    static::$kernel = new Kernel();
    static::$database = new DataBase\DataBase();
    static::$entity = new EntityManager();
  }

  /**
   * @param \Throwable $e
   *
   * @throws \App\Exceptions\InvalidRouteException
   */
  public function handleException(\Throwable $e) {
    if ($e instanceof \App\Exceptions\InvalidRouteException) {
      echo static::$kernel->launchAction('Error', 'error404', [$e]);
    }
    else {
      echo static::$kernel->launchAction('Error', 'error500', [$e]);
    }
  }
}