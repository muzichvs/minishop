<?php


namespace App;


use App\Controller\ErrorController;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

abstract class Builder {
  protected $twig;

  /**
   * Controller constructor.
   */
  public function __construct() {
    $loader = new FilesystemLoader(ROOT_PATH . DIRECTORY_SEPARATOR . 'templates');
    $this->twig = new Environment($loader);
  }

  /**
   * @param string $templateName
   * @param array $param
   *
   * @return string
   */
  protected function render(string $templateName, array $param = []) {
    $templateName.= '.twig';
    try {
      return $this->twig->render($templateName, $param);
    }

    catch (\Exception $exception) {
      return (new ErrorController())->error500();
    }
  }

  /**
   * @param string $templateName
   * @param array  $params
   *
   * @return string
   */
  protected function renderComponent(string $templateName, array $params = []) {
    return $this->render("component/$templateName", $params);
  }
}