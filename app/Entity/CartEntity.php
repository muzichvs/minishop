<?php

namespace App\Entity;

class CartEntity {
  const COOKIE_NAME = 'cart_products';

  /**
   * @param \App\Entity|\App\Entity\ProductEntity $product
   *
   * @return bool
   */
  public function addProduct(ProductEntity $product) {
    if($this->hasProduct($product)) {
      return FALSE;
    }

    $items = $this->getProductIds();
    $items[] = $product->getPid();

    $json_items = json_encode($items);
    $_COOKIE[self::COOKIE_NAME] = $json_items;
    return setcookie(self::COOKIE_NAME, $json_items, time() + 86400, '/');
  }

  /**
   * @param \App\Entity\ProductEntity $product
   *
   * @return bool
   */
  public function hasProduct(ProductEntity $product) {
    return in_array($product->getPid(), $this->getProductIds());
  }

  /**
   * @return array
   */
  public function getProductIds() {
    if(isset($_COOKIE[self::COOKIE_NAME])) {
      return json_decode($_COOKIE[self::COOKIE_NAME]);
    }

    return [];
  }

  /**
   * Clear cart
   */
  public function clear() {
    if(isset($_COOKIE[self::COOKIE_NAME])) {
      unset($_COOKIE[self::COOKIE_NAME]);
      setcookie(self::COOKIE_NAME, null, -1, '/');
    }
  }
}