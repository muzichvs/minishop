<?php


namespace App\Entity;


use App\Entity;

class ProductEntity extends Entity {
  private $pid;

  private $name;

  private $price;

  private $created;

  /**
   * @param int $id
   *
   * @return \App\Entity|\App\Entity\ProductEntity|null
   */
  public function load(int $id) {
    return parent::load($id);
  }

  /**
   * @return mixed
   */
  public function getPid() {
    return $this->pid;
  }

  /**
   * @param $id
   *
   * @return \App\Entity\ProductEntity
   */
  public function setPid($id): self {
    $this->pid = $id;

    return $this;
  }

  /**
   * @return mixed
   */
  public function getName() {
    return $this->name;
  }

  /**
   * @param $name
   *
   * @return \App\Entity\ProductEntity
   */
  public function setName($name): self {
    $this->name = $name;

    return $this;
  }

  /**
   * @return mixed
   */
  public function getPrice() {
    return $this->price;
  }

  /**
   * @param $price
   *
   * @return \App\Entity\ProductEntity
   */
  public function setPrice($price): self {
    $this->price = $price;

    return $this;
  }

  /**
   * @return mixed
   */
  public function getCreated() {
    return $this->created;
  }

  /**
   * @param $created
   *
   * @return \App\Entity\ProductEntity
   */
  public function setCreated($created): self {
    $this->created = $created;

    return $this;
  }
}