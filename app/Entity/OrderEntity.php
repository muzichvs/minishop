<?php

namespace App\Entity;

use App\App;
use App\Entity;

class OrderEntity extends Entity {

  const STATUS_NEW = 'new';
  const STATUS_PAID = 'paid';

  /**
   * @var \App\Storage\OrderStorage
   */
  protected $storage;

  private $oid;

  private $status;

  private $created;

  /**
   * @param int $id
   *
   * @return \App\Entity|\App\Entity\OrderEntity|null
   */
  public function load(int $id) {
    return parent::load($id);
  }

  /**
   * @return int|null
   */
  public function create() {
    $order_id = $this->storage->insert(self::STATUS_NEW);
    if(!$order_id) {
      return NULL;
    }

    foreach (App::$entity->cart()->getProductIds() as $product_id) {
      $this->storage->insertItem($order_id, $product_id);
    }

    return $order_id;
  }

  /**
   * @return array
   */
  public function getProducts() {
    $products = [];
    foreach ($this->storage->findItems($this->getOid()) as $item) {
      $products[] = App::$entity->product()->load($item['pid']);
    }

    return $products;
  }

  /**
   * @return $this
   */
  public function saveStatus() {
    $this->storage->updateStatus($this->getOid(), $this->getStatus());

    return $this;
  }

  /**
   * @return mixed
   */
  public function getOid() {
    return $this->oid;
  }

  /**
   * @param $id
   *
   * @return \App\Entity\OrderEntity
   */
  public function setOid($id): self {
    $this->oid = $id;

    return $this;
  }

  /**
   * @return mixed
   */
  public function getStatus() {
    return $this->status;
  }

  /**
   * @return string|null
   */
  public function getStatusName() {
    switch ($this->getStatus()) {
      case self::STATUS_NEW:
        return 'Новый';
      case self::STATUS_PAID:
        return 'Оплачен';
    }

    return NULL;
  }

  /**
   * @param $status
   *
   * @return \App\Entity\OrderEntity
   */
  public function setStatus($status): self {
    $this->status = $status;

    return $this;
  }

  /**
   * @return mixed
   */
  public function getCreated() {
    return $this->created;
  }

  /**
   * @param $created
   *
   * @return \App\Entity\OrderEntity
   */
  public function setCreated($created): self {
    $this->created = $created;

    return $this;
  }
}